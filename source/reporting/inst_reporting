#!/bin/sh
#
#  Installation script for the Grid Engine reporting module
#
#  Scriptname: inst_reporting
#
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2001 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__


AUTO=false
ORGPWD=`pwd`
umask 022

CLEAR=clear
$CLEAR

cd ..
PWD=`pwd`
. ./util/arch_variables
. ./util/install_modules/inst_common.sh
. ./util/install_modules/inst_qmaster.sh


EnterPW_oldmods=`stty -g`

BasicSettings
SetUpInfoText


. $ORGPWD/inst_util.sh

########################################################################
# Query SWC info:
#     - smcwebserver path           $SMCWEBSERVER
#     - SWC version                 $SWC_VERSION
#     - 3.0 version:    wcadmin     $WCADMIN
#                       smwebapp    $SMWEBAPP
# If any of required information is not found, or SWC version is not
# supported, installation of ARCo reporting fails.
# The list of supported SWC versions: 3.0
# Supported architectures: solaris, linux
########################################################################
querySWCInfo() {

   SMCWEBSERVER=`getSWCClientPath smcwebserver`
   if [ $? -eq 1 ]; then
      $INFOTEXT "smcwebserver: command not found. Make sure that the smcwebserver is on your PATH.\n"
      exit 1
   fi
   SWC_VERSION=`getSWCVersion`

   case $SWC_VERSION in
     "3.0" | "3.1")
        AS_USER_OPTION=" -u ";
        WCADMIN=`getSWCClientPath wcadmin`;
        if [ $? -eq 1 ]; then
           $INFOTEXT "wcadmin: command not found. Make sure that the wcadmin is on your PATH.\n"
           exit 1
        fi;
        SMWEBAPP=`getSWCClientPath smwebapp`;
        if [ $? -eq 1 ]; then
           $INFOTEXT "smwebapp: command not found. Make sure that the smwebapp is on your PATH.\n"
           exit 1
        fi;;
     *)
        $INFOTEXT "Not supported Sun Java Web Console version $SWC_VERSION.\n";
	exit 1;;
   esac

}

########################################################################
# get the path to the given SWC client
# first check if it is on the PATH, then explore the package information
# (rpm on linux, pkg on Solaris)
# if still not found, try the common path
########################################################################
getSWCClientPath() {

   if [ $1 = "" ]; then
      $INFOTEXT "Wrong usage of getSWCClientPath function. The client name must be specified."
      return 1
   else
      client="$1"
   fi

   if [ "$2" = "" ]; then
      SWCpkg=SUNWmcon
   else
      SWCpkg="$2"
   fi

   dummy=`which $client 2> /dev/null`
   if [ ! -s "$dummy" ]; then
      dummy=""
   fi
   if [ "$dummy" = "" ]; then
      case $ARCH in
         sol*)
            dummy_list=`pkgchk -l $SWCpkg 2> /dev/null | grep "Pathname" | grep "$client\$" | awk '{print $2}'`;;
         lx*)
            dummy_list=`rpm -ql $SWCpkg 2> /dev/null | grep -e "\/*\/$client\$"`;;
      esac
      dummy_list="$dummy_list /opt/sun/webconsole/bin/$client /usr/share/webconsole/bin/$client"

      for i in $dummy_list; do
         # test the client
         $i -V > /dev/null 2>&1
         if [ $? -eq 0 ]; then
            dummy=$i
            break
         fi
      done
   fi

   if [ "$dummy" = "" ]; then
      return 1
   else
      echo $dummy
      return 0
   fi
}

########################################################################
# Query the SWC ARCO_USER and his ARCO_GROUP
########################################################################
querySWCUser() {

   # first we check if the user is set in web console
   ARCO_USER=`$WCADMIN list -p 2> /dev/null | grep "com.sun.web.console.user" | awk '{print $2}'`

   # check if user exists in the system
   if [ "$ARCO_USER" != "" ]; then
      ARCO_USER=`getent passwd "$ARCO_USER" 2> /dev/null | awk -F: '{print $1}'`
   fi
   # if not, check if user noaccess exists
   if [ "$ARCO_USER" = "" ]; then
      ARCO_USER=`getent passwd "noaccess" 2> /dev/null | awk -F: '{print $1}'`
      if [ "$ARCO_USER" = "" ]; then
         # user noaccess doesn't exist, check if user nobody exists
         ARCO_USER=`getent passwd "nobody" 2> /dev/null | awk -F: '{print $1}'`
      fi
      if [ "$ARCO_USER" = "" ]; then
         # we still don't have ARCO_USER, let's ask
         dummy="noaccess"
         $INFOTEXT "\nEnter the user name to use for starting the "
         $INFOTEXT -n "Sun Java Web Console [$dummy] >> "
         ARCO_USER=`Enter $dummy`
         if [ "$ARCO_USER" = "" ]; then
            $INFOTEXT "\nThe ARCo user must be specified."
            exit 1
         else
            ARCO_USER=$dummy
         fi
      fi
   fi
   # find group for $ARCO_USER
   dummy=`getent passwd "$ARCO_USER" 2> /dev/null | awk -F: '{print $4}'`
   ARCO_GROUP=`getent group "$dummy" 2> /dev/null | awk -F: '{print $1}'`
}

########################################################################
# Create directory with ARCO_USER as owner
########################################################################
makeDirectory() {
  $INFOTEXT "Create directory $1"
  mkdir -p $1
  if [  $? -eq 0 ]; then
    chown $ARCO_USER:$ARCO_GROUP $1
    chmod 755 $1
    return 0
  fi
  return 1
}


########################################################################
# Query all parameters for the installation
########################################################################
queryParams() {

   ProcessSGERoot

   GetCell

   SetAdminUser

   queryJavaHome "1.5"

   $CLEAR

   SPOOL_DIR=/var/spool/arco

   $INFOTEXT -u "\nSpool directory"

   $INFOTEXT "\nIn the spool directory the @@GRID_ENGINE_NAME@@ reporting module will"
   $INFOTEXT "store all queries and results\n"

   dummy=$SPOOL_DIR
   $INFOTEXT -n "Enter the path to the spool directory [$dummy] >> "
   SPOOL_DIR=`Enter $dummy`

   QUERY_DIR=$SPOOL_DIR/queries
   RESULT_DIR=$SPOOL_DIR/results

   ########################

   $CLEAR
   $INFOTEXT -u "\nCluster Database Setup"

   # set the default database user
   DB_USER=arco_read

   $INFOTEXT -n \
               "\nEnter your database type ( o = Oracle, p = PostgreSQL ) [p] >> "
   DB_SELECT=`Enter p`

   CLUSTERS=""
   j=0
   while :
   do
     queryCluster
     #store cluster configuration
     eval DB_NAME_$j="$DB_NAME"
     eval DB_HOST_$j="$DB_HOST"
     eval DB_PORT_$j="$DB_PORT"
     eval DB_USER_$j="$DB_USER"
     eval DB_SCHEMA_$j="$DB_SCHEMA"
     eval DB_DRIVER_$j="$DB_DRIVER"
     eval DB_CLUSTER_$j="$DB_CLUSTER"
     eval DB_PW_$j="$DB_PW"

     CLUSTERS="$CLUSTERS $j"

     $INFOTEXT -ask y n -def n \
               -n "Do you want to add another cluster? (y/n) [n] >>"
        if [ $? -ne 0 ]; then
           break
        fi
     eval j=`expr $j + 1`

   done

   #######################

   $CLEAR
   $INFOTEXT -u "\nConfigure users with write access"

	APPL_USERS=$ADMINUSER
   while :
   do
       if [ "$APPL_USERS" != "" ]; then
          $INFOTEXT "\nUsers: $APPL_USERS"
       fi
       $INFOTEXT -n "Enter a user login name. (Hit <RETURN> to finish) >> "
       dummy=`Enter`
       if [ "$dummy" = "" ]; then
          break
       fi
       if [ "$APPL_USERS" = "" ]; then
          APPL_USERS="$dummy"
       else
         APPL_USERS="$APPL_USERS $dummy"
       fi
   done
}


########################################################################
# Query cluster
########################################################################
queryCluster() {
   while :
   do
     if [ "$DB_SELECT" = 'p' ]; then
         DB_DRIVER_JAR=$DBWRITER_PWD/lib/postgres-7.4.2.jar
         DB_TYPE="postgresql"
         queryPostgres 1
     elif [ "$DB_SELECT" = 'o' ]; then
         DB_TYPE="oracle"
         queryOracle 1
     fi

     queryDBSchema 1
     dummy=${DB_HOST}:${DB_NAME}:${DB_USER}
     $INFOTEXT -n "\nEnter the name of your cluster "
     $INFOTEXT "\n (it is recommended to use the same name as \$SGE_CLUSTER_NAME) [$dummy] >> "
     DB_CLUSTER=`Enter $dummy`

     $CLEAR
     $INFOTEXT -u "\nDatabase connection test"

     CP=$ORGPWD/WEB-INF/lib/arco_common.jar

     searchJDBCDriverJar $DB_DRIVER $ORGPWD/WEB-INF/lib

     CP=""

     for i in  $ORGPWD/WEB-INF/lib/*.jar; do
         if [ "$CP" = "" ]; then
            CP=$i
         else
            CP=$CP:$i
         fi
     done

     testDB
     if [ $? -eq 0 ]; then
        $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
        $CLEAR
        $INFOTEXT -u "\nDB parameters are now collected"

        $INFOTEXT "    CLUSTER_NAME=$DB_CLUSTER"
        $INFOTEXT "          DB_URL=$DB_URL"
        $INFOTEXT "         DB_USER=$DB_USER"

        $INFOTEXT -ask y n -def y -n "\nAre these settings correct? (y/n) [y] >> "
        if [ $? -eq 0 ]; then
           break;
        fi
     else
        $INFOTEXT -ask y n -def y \
                  -n "Do you want to repeat database connection setup? (y/n) [y] >>"
        if [ $? -ne 0 ]; then
           break
        fi
     fi
   done

}

########################################################################
# Remove old instances of the @@GRID_ENGINE_NAME@@ reporting module
########################################################################
removeOld() {

  names="reporting_ com.sun.grid.arco"

  for name in $names; do
     dummy=`$WCADMIN list -a | grep "$name" | awk '{print $1}'`

     if [ "$dummy" != "" ]; then
        app="$app $dummy"
     fi
  done

   if [ "$app" != "" ]; then

      $INFOTEXT -u "\n@@GRID_ENGINE_NAME@@ reporting module already registered at Sun Java Web Console"
      $INFOTEXT    "\nThe @@GRID_ENGINE_NAME@@ reporting modules can only be installed"
      $INFOTEXT    "if no previous version is registered."
      for i in $app; do
         $INFOTEXT -ask n y -def y -n \
                "\nShould the @@GRID_ENGINE_NAME@@ reporting module $i be unregistered? (y/n) [y] >> "

         if [ $? -ne 0 ]; then
            $WCADMIN undeploy -a "$i" -x "reporting"

            $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
         else
            $INFOTEXT "Can not install @@GRID_ENGINE_NAME@@ reporting module"
            exit 1
      fi

   done

   fi

}

########################################################################
# Copy example query to the spool directory if necessary
########################################################################
copyExampleQuery() {
   query=$1
   query_dir=$2
   copy_query=$3
   $INFOTEXT -n "Copy query `basename $query` ... "
   if [ $copy_query = "y" ]; then
       cp $query $query_dir
       res=$?
       if [ $res -eq 0 ]; then
           $INFOTEXT "OK"
       else
           $INFTEXT "Error $res"
       fi
    else
       $INFOTEXT "skipped"
    fi
}

createSpoolDir() {

   $INFOTEXT -u "\nInstall predefined queries"
   $INFOTEXT    "\n"
   if [ ! -d "$SPOOL_DIR" ]; then
      $INFOTEXT -ask y n -def y -n \
                "\nDirectory $SPOOL_DIR does not exist, create it? (y/n) [y] >> "
      if [ $? -eq 0 ]; then
        makeDirectory $SPOOL_DIR
        if [ $? -ne 0 ]; then
          $INFOTEXT "Can not create directory $SPOOL_DIR"
          exit 1
        fi
      else
        $INFOTEXT "Can not install the reporting module without a spool directory"
        exit 1
      fi
   fi

   QUERY_DIR=$SPOOL_DIR/queries
   if [ ! -d "$QUERY_DIR" ]; then
      makeDirectory $QUERY_DIR
      if [ $? -ne 0 ]; then
          $INFOTEXT "Can not create directory $QUERY_DIR"
         exit 1
      fi
   else
      $INFOTEXT "query directory $QUERY_DIR already exists"
   fi

   $INFOTEXT "Copy examples queries into $QUERY_DIR"

   if [ $DB_DRIVER = "oracle.jdbc.driver.OracleDriver" ]; then
      PREDEFINED_DIR=$ORGPWD/database/example_queries/oracle
   elif [ $DB_DRIVER = "org.postgresql.Driver" ]; then
      PREDEFINED_DIR=$ORGPWD/database/example_queries/postgres
   fi

   query_ask="y"
   dummy="n"
   result=""
   for i in $PREDEFINED_DIR/*.xml; do
     QUERY_FILE=`basename $i`
     if [ -f $QUERY_DIR/$QUERY_FILE ]; then
        # query already exists
        if [ $query_ask = "y" ]; then
            $INFOTEXT -n "Query $QUERY_FILE already exists. "
            $INFOTEXT -n "Overwrite? ( y = yes, n = no, Y = yes to all, N = no to all ) [$dummy] >> "
            result=`Enter $dummy`
        fi
        case $result in
             "y")
                 copyExampleQuery $i $QUERY_DIR "y";;
             "n")
                 copyExampleQuery $i $QUERY_DIR "n";;
             "Y")
                 copyExampleQuery $i $QUERY_DIR "y";
                 query_ask="n";;
	     "N")
                 copyExampleQuery $i $QUERY_DIR "n";
                 query_ask="n";;
        esac
     else
        # query doesn't exist, create it
        copyExampleQuery $i $QUERY_DIR "y"
     fi
   done

   chown $ARCO_USER:$ARCO_GROUP $QUERY_DIR/*.xml

   RESULT_DIR=$SPOOL_DIR/results
   if [ ! -d "$RESULT_DIR" ]; then
      makeDirectory $RESULT_DIR
      if [ $? -ne 0 ]; then
         $INFOTEXT "Can not create results directory in $SPOOL_DIR"
         exit 1
      fi
   fi
   $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
}

encryptPW() {

  DB_PW=`echo $1 | $JAVA_HOME/bin/java -cp $ORGPWD/WEB-INF/lib/reporting.jar com.sun.grid.arco.util.CryptoHandler`

}

getSWCVersion() {
   SWC_VERSION=`$SMCWEBSERVER -V | cut -d " " -f2`

   SWC_MAJOR=`echo $SWC_VERSION | cut -f1,1 -d .`
   SWC_MINOR=`echo $SWC_VERSION | cut -f2,2 -d .`
   echo "$SWC_MAJOR.$SWC_MINOR"
}

########################################################################
#
# create config.xml from
# config.xml.template
#
########################################################################
createConfigFile() {

   #temp files
   TMP_DATABASE_TEMPLATE=/tmp/database_template$$
   TMP_APPLUSERS_TEMPLATE=/tmp/applusers_template$$
   TMP_CONFIG_TEMPLATE=/tmp/config_template$$
   TMP_CONFIG_FILE=/tmp/config$$
   TMP_DATABASE_FILE=/tmp/database$$
   TMP_USER_FILE=/tmp/user$$

   #extract database section from template
   sed -n -e "/<database name/,/<\/database>/p" \
      $ORGPWD/util/config.xml.template > $TMP_DATABASE_TEMPLATE
   #extract appUser section from template
   sed -n -e "/<applUser>/,/<\/applUser>/p" \
      $ORGPWD/util/config.xml.template > $TMP_APPLUSERS_TEMPLATE
   #replace template sections by hook strings,
   # bug in Solaris, keep formating!!! Do not try to put it to one line
   sed -e '/<database name/,/<\/database>/c\
      DATABASES\
      '      -e '/<applUser>/,/<\/applUser>/c\
      APPUSERS\
      '  $ORGPWD/util/config.xml.template > $TMP_CONFIG_TEMPLATE


   # for every cluster definition
   for j in $CLUSTERS; do
   #restore cluster configuration variables
   eval DB_NAME="$"DB_NAME_$j
   eval DB_HOST="$"DB_HOST_$j
   eval DB_PORT="$"DB_PORT_$j
   eval DB_USER="$"DB_USER_$j
   eval DB_SCHEMA="$"DB_SCHEMA_$j
   eval DB_DRIVER="$"DB_DRIVER_$j
   eval DB_CLUSTER="$"DB_CLUSTER_$j
   eval DB_PW="$"DB_PW_$j

   #  encrypt the password
   DB_PW=`echo $DB_PW | $JAVA_HOME/bin/java -cp $ORGPWD/WEB-INF/lib/reporting.jar com.sun.grid.arco.util.CryptoHandler 2> /dev/null`

   #replace hook strings by variables and add to the database include file
   sed -e "s?DBNAME?$DB_NAME?g" \
		 -e "s?DBHOST?$DB_HOST?g" \
		 -e "s?DBPORT?$DB_PORT?g" \
		 -e "s?DBUSER?$DB_USER?g" \
		 -e "s?DBSCHEMA?$DB_SCHEMA?g" \
		 -e "s?DBDRIVER?$DB_DRIVER?g" \
		 -e "s?DBTYPE?$DB_TYPE?g" \
       -e "s?DBCLUSTER?$DB_CLUSTER?g" \
       -e "s?DBPWD?$DB_PW?g" \
       -e "/<\/database>/n" \
       $TMP_DATABASE_TEMPLATE >> $TMP_DATABASE_FILE
   done

   #replace hook strings by variables and add to the users include file
   for i in $APPL_USERS; do
      sed -e "s?APPLUSER?$i?g" \
          -e "/<\/applUser>/n" \
           $TMP_APPLUSERS_TEMPLATE >> $TMP_USER_FILE
   done

   #replace hook strings by include files
	sed -e "/DATABASES/r $TMP_DATABASE_FILE" \
	    -e "/DATABASES/d" \
       -e "/APPUSERS/r $TMP_USER_FILE" \
		 -e "/APPUSERS/d" \
		 -e "s?COMMON_PATH?$SPOOL_DIR?g" \
		  $TMP_CONFIG_TEMPLATE > $TMP_CONFIG_FILE

   ExecuteAsAdmin cp $TMP_CONFIG_FILE $SGE_ROOT/$SGE_CELL/arco/reporting/config.xml

   #clean up
   rm $TMP_USER_FILE $TMP_CONFIG_FILE $TMP_DATABASE_FILE
   rm $TMP_DATABASE_TEMPLATE $TMP_APPLUSERS_TEMPLATE $TMP_CONFIG_TEMPLATE

}

###############################################################
# Shuts down the WebConsole. Only used for Console < 3.0
###############################################################
shutDownConsole() {

   $INFOTEXT -u "Sun Java Web Console shutdown"
   $INFOTEXT -n "\nShutdown Sun Java Web Console ... "
   STOP_RESULT=`$SMCWEBSERVER stop`
   if [ $? -ne 0 ]; then
      $INFOTEXT " FAILED"
      $INFOTEXT  $STOP_RESULT
      exit 1
   else
      $INFOTEXT "OK"
      $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
   fi
}

###############################################################
# Starts the WebConsole.
###############################################################
startConsole() {

   $INFOTEXT -u "Sun Java Web Console startup"

   $INFOTEXT -n "\nStarting Sun Java Web Console ... "

   START_RESULT=`$SMCWEBSERVER start $AS_USER_OPTION $ARCO_USER`

   if [ $? -ne 0 ]; then
      $INFOTEXT "FAILED"
      echo $START_RESULT
   else
     $INFOTEXT "OK"
     $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
   fi
}

######################################################################
# Imports the SWC 3.0 files into the INSTALL_DIR
# If the application uses JSF components also add this command
# smwebapp import -f $INSTALL_DIR
######################################################################
importConsoleFiles() {

      $CLEAR;
      $INFOTEXT -u "\nImporting Sun Java Web Console 3.0 or 3.1 files"

      ExecuteAsAdmin $SMWEBAPP import -j $INSTALL_DIR
      $INFOTEXT "Imported files to $INSTALL_DIR"
      
      # create the product images
      ExecuteAsAdmin $SMWEBAPP prodname -t all -d $INSTALL_DIR
      
      $INFOTEXT "Created product images in $INSTALL_DIR/com_sun_web_ui/images"
      $INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
}

####################################################################
#  Create the file which depends on the version of SWC
#  Input  SWC_VERSION
####################################################################
createSWCFiles() {

   # Create the correct app.xml, and web.xml (depends on SWC Version)

   ExecuteAsAdmin \
         cp $ORGPWD/util/app-3.0.xml $SGE_ROOT/$SGE_CELL/arco/reporting/WEB-INF/app.xml
   ExecuteAsAdmin \
         cp $ORGPWD/util/web-3x.xml $SGE_ROOT/$SGE_CELL/arco/reporting/WEB-INF/web.xml
   INSTALL_DIR=$SGE_ROOT/$SGE_CELL/arco/reporting

}

# create the toc.xml file in the arco spool directory as default web console user noaccess
createTOCFile() {
  $INFOTEXT -n "Creating the TOC file ... "
  toc_result=`$JAVA_HOME/bin/java -Darco.home="$SGE_ROOT/$SGE_CELL/arco/reporting" \
               -Darco.config="$SGE_ROOT/$SGE_CELL/arco/reporting" \
               $JVMARGS -cp $CP com.sun.grid.arco.ArcoRun -l 2>/dev/null`
  if [ $? -ne 0 ]; then
    $INFOTEXT "FAILED\n"
  else
    chown $ARCO_USER $SPOOL_DIR/queries/toc.xml
    chgrp $ARCO_GROUP $SPOOL_DIR/queries/toc.xml
    $INFOTEXT "OK"
  fi
}

############################################################################
# Deploys the application in WebConsole 3.0 using the wcadmin command
############################################################################

deployIn30() {

   $WCADMIN deploy -a com.sun.grid.arco_@@ARCO_VERSION@@ -x reporting $TMP_INST_DIR/reporting
   $WCADMIN add -p -a com.sun.grid.arco_@@ARCO_VERSION@@ arco_config_file=$INSTALL_DIR/config.xml
   $WCADMIN add -p -a com.sun.grid.arco_@@ARCO_VERSION@@ arco_logging_level=INFO
   $WCADMIN add -p -a com.sun.grid.arco_@@ARCO_VERSION@@ arco_app_dir=$INSTALL_DIR
}

#############################################################################
#  Main
#############################################################################

case $ARCH in
   sol*) ;;
   lx*) ;;
   *)
      $INFOTEXT "Not supported architecture for ARCo reporting.";
      exit 1;;
esac

LicenseAgreement

$INFOTEXT -u "\nWelcome to the @@GRID_ENGINE_NAME@@ ARCo reporting module installation"

$INFOTEXT "The installation will take approximately 5 minutes\n"

$INFOTEXT -wait -n "Hit <RETURN> to continue >> "


euid=`$SGE_UTILBIN/uidgid -euid`

if [ "$euid" != "0" ]; then
  $INFOTEXT "reporting installation must be started as root"
  exit 1
fi



while :
do
  queryParams
  
  querySWCInfo
  querySWCUser

  $CLEAR
  $INFOTEXT -u "\nAll parameters are now collected"

  $INFOTEXT "       SPOOL_DIR=$SPOOL_DIR"
  $INFOTEXT "      APPL_USERS=$APPL_USERS"

  $INFOTEXT -ask y n -def y -n "\nAre these settings correct? (y/n) [y] >> "
   if [ $? -eq 0 ]; then
       break;
   fi

done

$CLEAR
removeOld

correctFilePermissions reporting $SGE_ROOT

$CLEAR
createSpoolDir

# ------------------------------------------------------------------------------
#  Setup the all files for reporting at $SGE_ROOT/$SGE_CELL/arco/reporting
# ------------------------------------------------------------------------------
$CLEAR
$INFOTEXT -u "\nARCo reporting module setup"


TMP_INST_DIR=$SGE_ROOT/$SGE_CELL/arco

if [ -d $TMP_INST_DIR/reporting ]; then
   $INFOTEXT "\nFound a previous installed version of the ARCo reporting"
   $INFOTEXT "modules at $TMP_INST_DIR"

   $INFOTEXT -ask y n -def y -n \
               "\nRemove directory ${TMP_INST_DIR}/reporting? (y/n) [y] >> "

   if [ $? -eq 0 ]; then
       ExecuteAsAdmin rm -rf $TMP_INST_DIR/reporting     
       $INFOTEXT "directory $TMP_INST_DIR/reporting removed"
   fi
else
  ExecuteAsAdmin mkdir -p $TMP_INST_DIR
fi

$INFOTEXT "Copying ARCo reporting file into $TMP_INST_DIR/reporting"

ExecuteAsAdmin cp -r $SGE_ROOT/reporting $TMP_INST_DIR

createConfigFile
createSWCFiles

$INFOTEXT "\nSetting up ARCo reporting configuration file. After registration of"
$INFOTEXT " the ARCo reporting module in the Sun Java Web Console you can find "
$INFOTEXT "this file at\n"
$INFOTEXT "      $INSTALL_DIR/config.xml\n"
$INFOTEXT -wait -n "\nHit <RETURN> to continue >> "

# Import the neccessary SWC 3.0 Files
importConsoleFiles

$CLEAR
$INFOTEXT -u "\nRegistering the @@GRID_ENGINE_NAME@@ reporting module in the Sun Java Web Console"

deployIn30

createTOCFile

$INFOTEXT -wait -n "\nHit <RETURN> to continue >> "
$CLEAR

SWC_STATUS=`$SMCWEBSERVER status`
if [ "$SWC_STATUS" = "Sun Java(TM) Web Console is stopped" ]; then
   startConsole
else
   $INFOTEXT -u "\nRestarting Sun Java Web Console"
   $SMCWEBSERVER restart $AS_USER_OPTION $ARCO_USER
fi

$INFOTEXT "@@GRID_ENGINE_NAME@@  ARCo reporting successfully installed\n"

