<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN" "http://java.sun.com/products/javahelp/helpset_1_0.dtd">
<helpset version="1.0">
 <title>
@@GRID_ENGINE_NAME@@ - Accounting and Reporting Console 
 </title>
 <maps>
  <homeID>
top
  </homeID>
  <mapref location="Map.jhm"/>

 </maps>
 <view>
  <name>
TOC
  </name>
  <label>
Table of Contents 
  </label>
  <type>
javax.help.TOCView
  </type>
  <data>
appTOC.xml
  </data>
 </view>
 <view>
  <name>
Index
  </name>
  <label>
Index 
  </label>
  <type>
javax.help.IndexView
  </type>
  <data>
appIndex.xml
  </data>
 </view>
 <view>
  <name>
Search
  </name>
  <label>
Search 
  </label>
  <type>
javax.help.SearchView
  </type>
  <data engine="com.sun.java.help.search.DefaultSearchEngine">
JavaHelpSearch
  </data>
 </view>
</helpset>
