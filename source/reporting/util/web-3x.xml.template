<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd">
<!--

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2007 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

For the most part, this file is just a normal web.xml file with some
additional restrictions. 

-->
<web-app>
   <context-param>
      <param-name>jato:com.sun.grid.arco.web.reportingmodule.*:moduleURL</param-name>
      <param-value>../reportingmodule</param-value>
   </context-param>
   <context-param>
      <param-name>jato:com.sun.grid.arco.web.arcomodule.*:moduleURL</param-name>
      <param-value>../arcomodule</param-value>
   </context-param>
   <!-- CONSOLE FILTER CONFIGURATION -->
   <!-- All management web applications must define this filter. -->
   <!-- Add ignore path parameter values for URI's for which the -->
   <!-- filter should pass through without making security checks. -->
   <!-- Paths can end with the "*" wildcard indicator. -->
   <filter>
      <filter-name>AppSessionManagerFilter</filter-name>
      <filter-class>
         com.sun.management.services.session.AppSessionManagerFilter
      </filter-class>
      <!-- ADD URI PATHS TO IGNORE WHEN USING THE SECURE PORT -->
      <init-param>
         <param-name>ignore-paths-https</param-name>
         <param-value>
            /images/*
            /html/*
            /version/*
         </param-value>
      </init-param>
      
      <!-- ADD URI PATHS TO IGNORE WHEN USING THE NON-SECURE PORT -->
      <init-param>
         <param-name>ignore-paths-http</param-name>
         <param-value>
            /help/*
            /images/*
         </param-value>
      </init-param>
   </filter>
   <filter-mapping>
      <filter-name>AppSessionManagerFilter</filter-name>
      <url-pattern>/*</url-pattern>
   </filter-mapping>
   <!-- ADD OPTIONAL APPLICATION FILTER DEFINITIONS HERE -->
   <!-- In order to intercept all inbound traffic, the filter must be mapped to /* -->
   <!-- CONSOLE WEB APPLICATION DEFAULT LISTENER -->
   <listener>
      <listener-class>com.sun.web.common.ConsoleServletBase</listener-class>
   </listener>
   
   <!-- Sun (TM) Java Web Console Registration Servlet   -->
   <!-- The registration servlet executes within a web app when the app  -->
   <!-- loads as the web server starts. The servlet reads the web app's   -->
   <!-- app.xml file and saves registration information in the registration  --> 
   <!-- service. The console Launch page uses the registration service to  -->
   <!-- determine the registered web applications that can be  -->
   <!-- launched and executed within the console instance.  -->
   <servlet>
      <servlet-name>Registrar</servlet-name>
      <servlet-class>com.sun.management.services.registration.servlet.WebConsoleRegistrarServlet
      </servlet-class>
      <load-on-startup>3</load-on-startup>
   </servlet>
   <!-- CONSOLE COMPLEX COMPONENT SERVLET DEFINITIONS -->
   <!-- Add the corresponding servlet definition tags if your application -->
   <!-- uses the component.   Add the corresponding servlet mapping tag -->
   <!-- in the mapping section later in this file.  Note the wizard component -->
   <!-- requires the specific application subclass name in its servlet-class -->
   <!-- tag.  There is one wizard servlet definition for each wizard instance. -->
   <!-- ACTION TABLE component -->
   <servlet>
      <servlet-name>tableServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.table.TableServlet
      </servlet-class>
   </servlet>
   <!-- BADGING component -->
   <servlet>
      <servlet-name>badgingServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.badging.BadgingServlet
      </servlet-class>
   </servlet>
   <!-- DATETIME component -->
   <servlet>
      <servlet-name>dateTimeServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.datetime.DateTimeServlet
      </servlet-class>
   </servlet>
   <!-- FILECHOOSER component -->
   <servlet>
      <servlet-name>fileChooserServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.filechooser.FileChooserServlet
      </servlet-class>
   </servlet>
   <!-- HELP component -->
   <servlet>
      <servlet-name>helpServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.help.HelpServlet
      </servlet-class>
   </servlet>
   <!-- HELP2 component -->
   <servlet>
      <servlet-name>help2Servlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.help2.Help2Servlet
      </servlet-class>
   </servlet>
   <!-- TOPOLOGY component -->
   <servlet>
      <servlet-name>ccTopologyServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.topology.CCTopologyServlet
      </servlet-class>
   </servlet>
   <!-- VERSION component -->
   <servlet>
      <servlet-name>VersionServlet</servlet-name>
      <servlet-class>
         com.sun.web.ui.servlet.version.VersionServlet
      </servlet-class>
   </servlet>
   <!-- WIZARD component - Change class path for each wizard instance -->
   <servlet>
      <servlet-name>wizardWindowServlet</servlet-name>
      <servlet-class>
         com.sun.web.admin.example.wizard.WizardServlet
      </servlet-class>
   </servlet>
   <!-- ADD APPLICATION SERVLET CONFIGURATION DEFINITIONS HERE -->
   <servlet>
      <servlet-name>ArcoServlet</servlet-name>
      <servlet-class>com.sun.grid.arco.web.arcomodule.ArcoServlet</servlet-class>
   </servlet>
   <servlet>
      <servlet-name>ExportResultServlet</servlet-name>
      <servlet-class>com.sun.grid.arco.web.arcomodule.ExportResultServlet</servlet-class>
   </servlet>
   <servlet>
      <servlet-name>ChartServlet</servlet-name>
      <servlet-class>com.sun.grid.arco.web.arcomodule.ChartServlet</servlet-class>
   </servlet>
   <!-- CONSOLE COMPLEX COMPONENT SERVLET MAPPINGS -->
   <!-- Add the corresponding component servlet mapping tags for the -->
   <!-- components used in the web application.  There must be a matching -->
   <!-- servlet definition tag in the previous section of this file. -->
   <!-- ACTION TABLE component -->
   <servlet-mapping>
      <servlet-name>tableServlet</servlet-name>
      <url-pattern>/table/*</url-pattern>
   </servlet-mapping>
   <!-- BADGING component -->
   <servlet-mapping>
      <servlet-name>badgingServlet</servlet-name>
      <url-pattern>/badging/*</url-pattern>
   </servlet-mapping>
   <!-- DATETIME component -->
   <servlet-mapping>
      <servlet-name>dateTimeServlet</servlet-name>
      <url-pattern>/datetime/*</url-pattern>
   </servlet-mapping>
   <!-- FILECHOOSER component -->
   <servlet-mapping>
      <servlet-name>fileChooserServlet</servlet-name>
      <url-pattern>/filechooser/*</url-pattern>
   </servlet-mapping>
   <!-- HELP component -->
   <servlet-mapping>
      <servlet-name>helpServlet</servlet-name>
      <url-pattern>/cchelp/*</url-pattern>
   </servlet-mapping>
   <!-- HELP2 component -->
   <servlet-mapping>
      <servlet-name>help2Servlet</servlet-name>
      <url-pattern>/cchelp2/*</url-pattern>
   </servlet-mapping>
   <!-- TOPOLOGY component -->
   <servlet-mapping>
      <servlet-name>ccTopologyServlet</servlet-name>
      <url-pattern>/ccTopologyImage</url-pattern>
   </servlet-mapping>
   <!-- VERSION component -->
   <servlet-mapping>
      <servlet-name>VersionServlet</servlet-name>
      <url-pattern>/ccversion/*</url-pattern>
   </servlet-mapping>
   <!-- WIZARD component -->
   <servlet-mapping>
      <servlet-name>wizardWindowServlet</servlet-name>
      <url-pattern>/wizard/*</url-pattern>
   </servlet-mapping>
   <!-- The mapping for the registration servlet  -->
   <servlet-mapping>
      <servlet-name>Registrar</servlet-name>
      <url-pattern>/Registrar</url-pattern>
   </servlet-mapping>
   <servlet-mapping>
      <servlet-name>ArcoServlet</servlet-name>
      <url-pattern>/arcomodule/*</url-pattern>
   </servlet-mapping>
   <servlet-mapping>
      <servlet-name>ExportResultServlet</servlet-name>
      <url-pattern>/export/*</url-pattern>
      <!-- <url-pattern>/reportingModule/*</url-pattern>-->
   </servlet-mapping>
   <servlet-mapping>
      <servlet-name>ChartServlet</servlet-name>
      <url-pattern>/chart_servlet</url-pattern>
      <!-- <url-pattern>/reportingModule/*</url-pattern>-->
   </servlet-mapping>
   <session-config>
      <session-timeout>30</session-timeout>
   </session-config>
   <taglib>
      <taglib-uri>/WEB-INF/tld/com_sun_web_ui/cc.tld</taglib-uri>
      <taglib-location>/WEB-INF/tld/com_sun_web_ui/cc.tld</taglib-location>
   </taglib>
   <taglib>
      <taglib-uri>/WEB-INF/tld/com_iplanet_jato/jato.tld</taglib-uri>
      <taglib-location>/WEB-INF/tld/com_iplanet_jato/jato.tld</taglib-location>
   </taglib>
</web-app>
