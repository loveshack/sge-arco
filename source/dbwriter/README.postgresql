Setting up dbwriter with PostgreSQL
-----------------------------------

This covers the procedure for setting up a PostgreSQL database for use
with dbwriter and installing dbwriter.  It needs extending to cover
multi-cluster setup.

We'll assume PostgreSQL is installed and running, either by installing
a package available for your operating system or by getting it from
http://www.postgresql.org/ and building it yourself.  (Make sure the
database is vacuumed, e.g. by turning on "autovacuum" in
postgresql.conf with recent versions.)  We also assume there is a
"postgres" user on the system where it's running, and we can
administer the database while logged in as postgres.  It is convenient
to run it on the qmaster machine if you don't already have it running
elsewhere.

The initial setup is done on the database server.

The Java connector insists on talking over a network socket (not a
Unix domain one) so It's necessary to have authentication configured
for such access.  E.g. in the pg_hba.conf file, have a line like

  host    all     all     127.0.0.1/32    md5

or, specifically for the arco database

  host    arco     all     127.0.0.1/32    md5

controlling access to localhost.  (Use a different network range as
appropriate if the server is on a different host to the qmaster).
Note that it has to be an address block (address with CIDR netmask).
Also the columns have to be separated by a tab, not spaces.

Enable network access in the postgresql.conf using, say,

  listen_addresses = 'localhost'
  port = 5432

for connexion from localhost on the Postgres standard port (5432).

Now create the database and two users for accessing it.  One,
conventionally "arco_write" will have write permission:

  # su - postgres
  $ createuser -P -S -d -R arco_write
  Enter password for new role: 
  Enter it again: 
  CREATE ROLE

and another will have only read permission:

  $ createuser -P -S -D -R arco_read
  Enter password for new role: 
  Enter it again: 
  CREATE ROLE

The write user needs permission to create tabalespaces:

  $ psql
  Welcome to psql 8.1.23, the PostgreSQL interactive terminal.
  
  Type:  \copyright for distribution terms
         \h for help with SQL commands
         \? for help with psql commands
         \g or terminate with semicolon to execute query
         \q to quit
  
  postgres=# grant create on tablespace pg_default to arco_write;
  GRANT
  postgres=# \q
  $ createdb -O arco_write arco
  CREATE DATABASE
  $ logout

If you need to remove the user eventually, use something like:

  revoke all privileges on tablespace pg_default from user arco_write;

The following applies to setup on the qmaster machine.

For a new installation, unpack the dbwriter distribution on the
qmaster, e.g. into /opt/sge/dbwriter, and copy or link the appropriate
Java database connector into the dbwriter/lib directory.  E.g.

  # ln -s /usr/share/java/postgresql-jdbc.jar /opt/sge/dbwriter/lib

Ensure you have sourced the SGE settings file to define SGE_ROOT and
SGE_CELL, cd into the dbwriter directory and run the "inst_dbwriter"
script to do the installation.  You should be able mostly to use the
defaults, but you will need the Java location, e.g. "/usr" if the
"java" command is in /usr/bin.  If the postgres server runs on the
qmaster, use "localhost" for the database host.  Say "y" to upgrading
the database model.

If uprading, do the following:  When the qmaster is shut down for the
upgrade, ensure all the reporting data are processed; then there
should be no $SGE_ROOT/$SGE_CELL/common/reporting{,.processing}
files.  Complete the SGE upgrade and re-install dbwriter with

  # ./inst_dbwriter -upd

For information on the database schema, needed to make queries,
consult dbwriter/database/postgres/dbdefinition.xml.
