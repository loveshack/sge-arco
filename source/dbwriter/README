This is a version of the dbwriter tool for maintaining a database of
Grid Engine reporting information from the Son of Grid Engine project
<http://arc.liv.ac.uk/SGE>.

The dbwriter daemon reads the SGE "reporting" file which is generated
by setting "reporting=true" in the qmaster "reporting_params".  You
may want to configure reporting_params more extensively (see qconf(1),
sge_conf(5)) and add values to "report_variables" in the global or
local host configuration (see host_conf(5)).

See README.postgresql for information on setting it up with a
PostgresSQL database.

There is currently no support for querying the database.  (The old
ARCo display used the proprietary Webconsole
<https://arc.liv.ac.uk/trac/SGE/ticket/1325>, and the "arcorun"
command-line tool needs dependencies on Webconsole fixing.)  Example
SQL for queries can be extracted from the files in
http://arc.liv.ac.uk/repos/darcs/arco/source/reporting/database/example_queries/postgres/
and http://arc.liv.ac.uk/repos/darcs/arco/www/contrib/arco_queries.tar.gz
They may be useful for setting up direct database queries or for use with
another tool.

Some other tools are available for processing the accounting data (not
the full reporting data).  See <http://arc.liv.ac.uk/SGE/tools.html>.
